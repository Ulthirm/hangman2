package javaapplication19;

import java.util.*;
/**
 * Title: Hangman.java
 * Abstract: Program for the user to play hangman.
 * Author: Joseph Molina
 * ID: 3388
 * Date: 2/9/15
 *
 */
public class Hangman {

    private static String guess2;
    private static HangmanFrame HM = new HangmanFrame();
    public static int GameLocationIndex = 0;
	public static void main(String[] args) {
		String[] Hangman = {
                          " ______________\n"  
                        + "     |   /    |\n "  
                        + "     |  /    ☺\n"
                        + "     |/      /|\\\n" 
                        + "     |       / \\\n"  
                        + "     |\n" 
                        + "__|___________\n"
                        ,
                          " ______________\n" 
                        + "     |   /    |\n" 
                        + "     |  /    ☺\n" 
                        + "     |/       |\\\n" 
                        + "     |       / \\\n"
                        + "     |\n"
                        + "__|___________\n"
                        ,
                          " ______________\n"
                        + "     |   /    |\n"
                        + "     |  /    ☺\n" 
                        + "     |/       |\n"
                        + "     |       / \\\n"
                        + "     |\n"
                        + "__|___________\n"
                        ,
                          " ______________\n"
                        + "     |   /    |\n"
                        + "     |  /    ☺\n"
                        + "     |/       |\n"
                        + "     |       / \n"
                        + "     |\n"
                        + "__|___________\n"
                        ,
                          " ______________\n"
                        + "     |   /    |\n"
                        + "     |  /    ☺\n"
                        + "     |/       |\n"
                        + "     |         \n"
                        + "     |\n"
                        + "__|___________\n"
                        ,
                          " ______________\n"
                        + "     |   /    |\n"
                        + "     |  /    ☺\n"
                        + "     |/       \n"
                        + "     |        \n"
                        + "     |\n"
                        + "__|___________\n"
                        ,
                          " ______________\n"
                        + "     |   /    |\n"
                        + "     |  /     \n"
                        + "     |/       \n"
                        + "     |        \n"
                        + "     |\n"
                        + "__|___________\n" };
                
                HM.setVisible(true);
                
		//String variables
		String word = getWord();
		String guess = "";
                String guess2 = "";
		
                word.toLowerCase();
                
              
                
                
		//Integer variables.
                int body = 6;
		int guesses = 6;
		int filled = 0;
		int place = 0;
		int choice = 1;
		
		//Scanner object
		Scanner keyboard = new Scanner(System.in);
		
		//Character variables.
		char white = '_';
		//char hint;
		char char1 = '\0';
		
		//boolean variables.
		boolean atLeastAlpha = guess.matches(".*[a-zA-Z]+.*");
		boolean valid = false;
		boolean isThere = false;
		
		
		//Putting the string into an array list, to later compare values to.
		ArrayList<Character> charlist = new ArrayList<Character>();
		for(int i = 0; i < word.length(); i++)
		{
			charlist.add(Character.valueOf(word.charAt(i)));
		}
		
		
		//Creating the game board
		ArrayList<Character> board = new ArrayList<Character>();
		for(int i = 0; i < word.length();i++)
		{
			if(Character.isWhitespace(word.charAt(i)))
				board.add('#');
			else
			{
				board.add('_');
			}
				
		}
		
		//Outputting the gameboard
                //System.out.println(Hangman[body]);
                HM.HangLife.setText(Hangman[body]);
                //HM.IO.append("test");
		HM.IO.append("\nSo far, the word is:     ");
		for(int i = 0; i < board.size(); i++)
		{	
			HM.IO.append(board.get(i) + " ");
		}
		
			//Main Game Loop
		while(guesses != 0){
			
			//A do-while loop to validate a correct choice
			do{
				try{
					
					HM.IO.append("\n");
					HM.IO.append("\nYou have " + guesses + " incorrect guesses left");
					valid = true;
						
				}
				catch(Exception e)
				{
					keyboard.nextLine();
					HM.IO.append("\nIncorrect input");
				}
			}while(valid != true);
			
			
			
			//loop for if the user chose 1
			if(choice == 1)
			{
				
				
				//making sure the length is of size 1 and it is only a letter
				while(guess.length() != 1 && atLeastAlpha == false)
				{	
					//keyboard.nextLine();
					System.out.println();
                                        

                                        
                                            
                                        
                                    
//                                        //continue;
//                                    }
					HM.IO.append("\nEnter your guess: ");
//					try {
//                                        System.out.print(Hangman[guesses]);
//                                    } catch (Exception e) {
//                                        //continue;
//                                    }
					guess = keyboard.nextLine();
					guess2 = guess.toLowerCase();
					char1 = guess2.charAt(0);
                                        
                                        
					//if the conditions are not meet, loop it
					if(guess2.length() > 1)
					{	
						keyboard.nextLine();
						HM.IO.append("\nIncorrect input");
					}
					
				
					//System.out.println("input is:" + guess);
				}
				
				
				
				//checking if the letter is the arraylist and saving the index
				for(int i = 0; i < charlist.size();i++){
					
					if(charlist.get(i).equals(new Character(Character.toLowerCase(char1))))
					{
						 board.set(i, char1);
						isThere = true;
						filled++;
						
					}
					
				}
				
				
				//if 'isThere' is false, that means the letter is not part of the actual word
				if(isThere == false)
				{
                                    body--;
                                    HM.HangLife.setText(Hangman[body]);
					HM.IO.append("\nSorry " + guess2 + " isn't part of the word");
					guesses = guesses - 1;
					if(guesses == 0)
					{
                                            
						HM.IO.append("\nYou failed. The word was ");
						for(int i = 0; i < charlist.size();i++)
						{
							HM.IO.append(charlist.get(i).toString());
						}
						
						ender();
					}
					
				}//Else statement to print to the user notifying them that their guess is part of the word.
				else if(isThere == true)
				{
                                    HM.HangLife.setText(Hangman[body]);
					HM.IO.append("\nThat's right " + guess2 + " is in the word!     ");
				}
				
			
				
				//replacing the index of the board with the letter
				
				for(int i = 0; i < board.size(); i++)
				{	
					HM.IO.append(board.get(i) + " ");
				}
				//If statement to check 
				if(filled == charlist.size())
				{
					HM.IO.append("\n");
					HM.IO.append("\nCongratulations!");
					
					for(int i = 0; i < board.size(); i++)
					{	
						HM.IO.append(board.get(i) + " ");
					}
					
					HM.IO.append("\n is the word!");
					
					ender();
				}
			}
			
			//Resetting back the variables to their default values.
			isThere = false;
			guess = "";
			guess2 = "";
			//If statement if the user chooses the second choice.
			//If statement to handle the hints the user has.
		     
				
		}
		
	}
	
//Method to get user input
public static String getWord(){
		
		HM.IO.append("--------- Welcome to Hangman -------0.2");
		Scanner input = new Scanner(System.in);
		HM.IO.append("\nEnter a word: ");
               String wordin = input.nextLine();
               String wordinputlower = wordin.toLowerCase();
               GameLocationIndex = 1;
	    return wordinputlower;
	}

//Method to exit out of the program.
public static void ender()
{
	System.exit(0); 
}
public void editText(){
                //
}
}